# -*- coding: utf-8 -*-
import config
import urllib.request
import datetime
import mindy.graph
import time
import traceback
import requests


def splitNotice(self, last_result, statement, user):
    stat = statement.split('.')
    if len(stat) > 1:
        for i in stat:
            if 'SetRemove' in i:
                notice(self, last_result, i[len('SetRemove' + "('"):-2], 'setremove', '')
                continue
            elif 'IncOrSet' in i:
                continue
            elif 'Set' in i:
                stat_new = i[4:-1].replace("'", '').split(',')
                if stat_new[0] == 'ответственный' and last_result is not mindy.graph.basics.NodeList:
                    notice(self, last_result, stat_new[0], 'set', last_result['ответственный'])
                elif len(stat_new) == 3:
                    notice(self, last_result, stat_new[0], 'set', stat_new[2].replace('(', '("').replace(')', '")'))
                else:
                    notice(self, last_result, stat_new[0], 'set', stat_new[1].replace('(', '("').replace(')', '")'))
                continue
    else:
        pass
#        print('!!!!!!!!!!!', stat, '!!!!!!!!!!!!!!!')


def notice(self, node, current_user, source, current_user2, segm=''):
    hostName = config.config_pages['host']
#    print(hostName)
    def XMPP(login, mss, tf=False):
#        print('>>>>>>>>>>>>XMPP<<<<<<<<<<<<<<<<<<')
        user_p = login
        if 'telephone' in user_p and tf:
            headers = {'login': 'Lafaet', 'password': 'dc7500rs'}
            user_p['telephone'] = user_p['telephone'].replace(' ', '')
            print("http://api.smsfeedback.ru/messages/v2/send/" + '?phone=%2B' + user_p[
                'telephone'] + '&text=' + urllib.parse.quote_plus(mss.encode("utf8").strip()))
            try:

                url = "http://api.smsfeedback.ru/messages/v2/send/" + '?phone=%2B' + user_p[
                    'telephone'] + '&text=' + urllib.parse.quote_plus(mss.encode("utf8").strip())
                r1 = requests.get(url, headers=headers)
                r2 = r1.read()
                #print('read', r2)
            except:
                traceback.print_exc()
            url = "http://api.smsfeedback.ru/messages/v2/statusQueue/?statusQueueName=queue1&limit=5"
            r2 = requests.get(url, headers=headers)
            #print(r2.read())
        return

    if source == 'раздел':
        if 'type' in node and 'имя' in node and 'номер' in node:
            name = node['type'] + ' ' + node['имя'] + ' с номером ' + node['номер'] + ' перенесена в другой ' + source
            if type(self) is mindy.graph.basics.Node:
                self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                          'получатель': current_user['имя'], 'прочитано': 'нет'}).AddParent(current_user)
                self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                          'получатель': current_user2['имя'], 'прочитано': 'нет'}).AddParent(current_user2)
            else:
                self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                              'получатель': current_user['имя'], 'прочитано': 'нет'}).AddParent(current_user)
                self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                              'получатель': current_user2['имя'], 'прочитано': 'нет'}).AddParent(current_user2)
        else:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!')
            print(node)
            print('!!!!!!!!!!!!!!!!!!!!!!!!!')
    elif source == 'set':
        try:
          if type(node) == "mindy.graph.basics.NodeList" and len(node) > 0:
              current = node[0].parents({})
          else:
              current = node.parents({})
          if current_user == 'исполнитель':
            if 'type' in node and 'имя' in node and 'номер' in node:
                name = node['type'] + ' ' + node['имя'] + ' с номером ' + node['номер'] + ', назначена вам'
                user_name = node.parent_graph.MatchOne(
                    {'_segment': node['_segment'], 'type': 'пользователь', 'имя': current_user2})
                if type(self) is mindy.graph.basics.Node:
                    self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                              'получатель': user_name['имя'], 'прочитано': 'нет'}).AddParent(user_name)
                else:
                    self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                  'получатель': user_name['имя'], 'прочитано': 'нет'}).AddParent(user_name)
                if node['type'] == 'проблема':
                    XMPP(user_name, name, True)
                else:
                    XMPP(user_name, name)
                for x in current:
                    if x['type'] == '_создатель':
                        name1 = node['type'] + ' ' + node['имя'] + ' с номером ' + node[
                            'номер'] + ', назначен исполнитель'
                        x1 = x.parents({'type': 'пользователь'})[0]
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name1,
                                      'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name1,
                                          'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x)
                        XMPP(x1, name)
            else:
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
                print(self)
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
          elif current_user == 'статус' and current_user2 == 'в работе':
            if 'type' in node and 'имя' in node and 'номер' in node:
                name = node['type'] + ' ' + node['имя'] + ' с номером ' + node[
                    'номер'] + ', изменено поле ' + current_user + ' на ' + str(current_user2)
                for x in current:
                    if x['type'] == 'пользователь':
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                      'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x)
                        # XMPP(x,name,True)
                    elif x['type'] == '_создатель':
                        x1 = x.parents({'type': 'пользователь'})[0]
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                      'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x)
                        XMPP(x1, name)
            else:
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
                print(self)
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
          elif current_user == 'приоритет':
            print('приоритет')
            t = time.time()
            if 'type' in node and 'имя' in node and 'номер' in node:
                name = node['type'] + ' ' + node['имя'] + ' с номером ' + node['номер'] + ', повышен приоритет'
                for x in current:
                    if x['type'] == 'пользователь':
                        ch = x.children({'type': 'задача', 'статус': 'не выполнен'}) + x.children(
                            {'type': 'проблема', 'статус': 'не выполнен'})
                        k = 0
                        for i in ch:
                            if 'приоритет' in i and float(i['приоритет']) > float(k):
                                k = float(i['приоритет'])
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                      'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                        print('>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', k, float(current_user2), float(i['приоритет']))
                        if float(current_user2) > k and float(i['приоритет']) < k:
                            XMPP(x, name, True)
                        else:
                            XMPP(x, name)
                    elif x['type'] == '_создатель':
                        x1 = x.parents({'type': 'пользователь'})[0]
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                      'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x1)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x1)
                        XMPP(x1, name, True)
            else:
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
                print(self)
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
          elif current_user == 'сделать_к' or current_user == 'начало_выполнения' or current_user == 'срок_выполнения':
            current_user2 = eval(str(current_user2))
            if 'type' in node and 'имя' in node and 'номер' in node:
                name = node['type'] + ' ' + node['имя'] + ' с номером ' + node[
                    'номер'] + ', изменено поле ' + current_user.replace('_', ' ') + ' на ' + str(
                    datetime.datetime.fromtimestamp(float(current_user2)).strftime("%d %B %Y, %H:%M"))
                for x in current:
                    if x['type'] == 'пользователь':
                        ch = x.children({'type': 'задача', 'статус': 'не выполнен'}) + x.children(
                            {'type': 'проблема', 'статус': 'не выполнен'})
                        k = 0
                        for i in ch:
                            if 'приоритет' in i and float(i['приоритет']) > float(k):
                                k = float(i['приоритет'])
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                      'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                    elif x['type'] == '_создатель':
                        x1 = x.parents({'type': 'пользователь'})[0]
                        if type(self) is mindy.graph.basics.Node:
                            self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                      'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x1)
                        else:
                            self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x1)
                        XMPP(x1, name)
            else:
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
                print(self)
                print('!!!!!!!!!!!!!!!!!!!!!!!!!')
          else:
            if current_user[0] != '_':
                if 'type' in node and 'имя' in node and 'номер' in node:
                    name = node['type'] + ' ' + node['имя'] + ' с номером ' + node[
                        'номер'] + ', изменено поле ' + current_user + ' на ' + str(current_user2).replace("'", "\'")
                    for x in current:
                        if x['type'] == 'пользователь':
                            if type(self) is mindy.graph.basics.Node:
                                self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                            else:
                                self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                              'получатель': x['имя'], 'прочитано': 'нет'}).AddParent(x)
                            XMPP(x, name)
                        elif x['type'] == '_создатель':
                            x1 = x.parents({'type': 'пользователь'})[0]
                            if type(self) is mindy.graph.basics.Node:
                                self.Add({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                          'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x1)
                            else:
                                self.AddNode({'type': 'уведомление', '_segment': node['_segment'], 'имя': name,
                                              'получатель': x1['имя'], 'прочитано': 'нет'}).AddParent(x1)
                            XMPP(x1, name)
                else:
                    print('!!!!!!!!!!!!!!!!!!!!!!!!!')
                    print(self)
                    print('!!!!!!!!!!!!!!!!!!!!!!!!!')
        except:
              print(type(node))
    elif source == 'setremove':
        current = node.parents({})
        if 'type' in node and 'имя' in node and 'номер' in node:
            name = node['type'] + ' ' + node['имя'] + ' с номером ' + node['номер'] + ', изменен тип на ' + current_user
            for x in current:
                if x['type'] == 'пользователь':
                    if type(self) is mindy.graph.basics.Node:
                        self.Add(
                            {'type': 'уведомление', '_segment': node['_segment'], 'имя': name, 'получатель': x['имя'],
                             'прочитано': 'нет'}).AddParent(x)
                    else:
                        self.AddNode(
                            {'type': 'уведомление', '_segment': node['_segment'], 'имя': name, 'получатель': x['имя'],
                             'прочитано': 'нет'}).AddParent(x)
                    XMPP(x, name)
                elif x['type'] == '_создатель':
                    x1 = x.parents({'type': 'пользователь'})[0]
                    if type(self) is mindy.graph.basics.Node:
                        self.Add(
                            {'type': 'уведомление', '_segment': node['_segment'], 'имя': name, 'получатель': x1['имя'],
                             'прочитано': 'нет'}).AddParent(x1)
                    else:
                        self.AddNode(
                            {'type': 'уведомление', '_segment': node['_segment'], 'имя': name, 'получатель': x1['имя'],
                             'прочитано': 'нет'}).AddParent(x1)
                    XMPP(x1, name)
        else:
            print('!!!!!!!!!!!!!!!!!!!!!!!!!')
            print(node)
            print('!!!!!!!!!!!!!!!!!!!!!!!!!')
    return
